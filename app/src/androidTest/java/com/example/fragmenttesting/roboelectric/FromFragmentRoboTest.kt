package com.example.fragmenttesting.roboelectric

import android.net.Uri
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.semantics.SemanticsProperties
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.filters.MediumTest
import com.example.fragmenttesting.MainViewModel
import com.example.fragmenttesting.MainScreenState
import com.example.fragmenttesting.R
import com.example.fragmenttesting.UserModel
import com.example.fragmenttesting.from.FromFragment
import com.example.fragmenttesting.utils.launchFragmentInHiltContainer
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
//@Config(application = HiltTestApplication.class)
@MediumTest
class FromFragmentRoboTest {

    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    var composeTestRule = createComposeRule()

    //@BindValue //added to replace actual viewModel
    val viewModel = mockk<MainViewModel>(relaxed = true)

    private val mockNavController = mockk<NavController>(relaxed = true)

    private val screenState = MutableLiveData<MainScreenState>()

    private val errorMessage = "Something went wrong. Please try again later."

    @Before
    fun setUp(){
        hiltRule.inject()
    }

    @Test
    fun testFragmentWithSuccessScenario() {
        val dummyString = "qwerty"
        viewModel.fetchData(true)

        every { viewModel.screenState } returns screenState

        screenState.postValue(MainScreenState.Success(UserModel("John Doe", 20, "abc@gmail.com")))

        launchFragmentInHiltContainer<FromFragment> {
            mockNavController.setGraph(R.navigation.nav_graph)
            Navigation.setViewNavController(requireView(), mockNavController)

        }

        val btn = SemanticsMatcher.expectValue(
            SemanticsProperties.Role, Role.Button
        )

        composeTestRule.onNodeWithTag("EditTextTestTag").assertExists()
            .performTextInput(dummyString)
        composeTestRule.onNode(btn).assertExists().performClick()

        val deepLink = Uri.parse("myapp://to/$dummyString")

        verify { mockNavController.navigate(deepLink) }
    }

    @Test
    fun testFragmentWithErrorScenario() {

        viewModel.fetchData(false)
        every { viewModel.screenState } returns screenState

        screenState.postValue(MainScreenState.Error(errorMessage))

        launchFragmentInHiltContainer<FromFragment> {
            mockNavController.setGraph(R.navigation.nav_graph)
            Navigation.setViewNavController(requireView(), mockNavController)
        }

        composeTestRule.onNodeWithText(errorMessage).assertExists().assertIsDisplayed()
    }

    @After
    fun tearDown(){
        unmockkAll()
    }
}