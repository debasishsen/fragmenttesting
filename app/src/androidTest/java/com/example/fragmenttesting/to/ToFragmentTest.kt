package com.example.fragmenttesting.to

import android.os.Bundle
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.semantics.SemanticsProperties
import androidx.compose.ui.test.SemanticsMatcher
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.performClick
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.example.fragmenttesting.R
import com.example.fragmenttesting.detail.DetailFragment
import com.example.fragmenttesting.utils.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain

@HiltAndroidTest
class ToFragmentTest {

//    @get:Rule
//    var rule = RuleChain
//        .outerRule(HiltAndroidRule(this))
//        .around(createComposeRule())

    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    var composeTestRule = createComposeRule()

    private val mockNavController = mockk<NavController>(relaxed = true)

    @Test
    fun testToFragmentLaunchWithFragmentArgs(){
        val fragmentArgs = bundleOf("text" to "123456")
        launchFragmentInHiltContainer<ToFragment>(fragmentArgs = fragmentArgs){
            mockNavController.setGraph(R.navigation.nav_graph)
            Navigation.setViewNavController(requireView(), mockNavController)
        }

        val btn = SemanticsMatcher.expectValue(
            SemanticsProperties.Role, Role.Button
        )
        composeTestRule.onNode(btn).assertExists().performClick()
        verify { mockNavController.navigate(R.id.detailFragment) }
    }
}