package com.example.fragmenttesting.detail

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.filters.MediumTest
import com.example.fragmenttesting.R
import com.example.fragmenttesting.utils.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
@MediumTest
class DetailFragmentTest {

    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    var composeTestRule = createComposeRule()

    @Before
    fun setUp(){
        hiltRule.inject()
    }

    @Test
    fun testDetailFragmentLaunch(){
        val navController = TestNavHostController(
            ApplicationProvider.getApplicationContext()
        )
        launchFragmentInHiltContainer<DetailFragment>{
            navController.setGraph(R.navigation.nav_graph)
            navController.setCurrentDestination(R.id.detailFragment)
            Navigation.setViewNavController(requireView(), navController)
        }
        composeTestRule.onNodeWithText("Details Fragment").assertExists().assertIsDisplayed()

        Assert.assertEquals(navController.currentDestination?.id, R.id.detailFragment)

    }
}