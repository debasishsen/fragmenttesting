package com.example.fragmenttesting

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(): ViewModel(){

    private val repository: MainRepository = MainRepositoryImpl()
    private val defaultError = "Something went wrong. Please try again later."
    val screenState = MutableLiveData<MainScreenState>()

    fun fetchData(status: Boolean) {
        if (screenState.value is MainScreenState. Success) return
        screenState.value = MainScreenState.Loading
        viewModelScope.launch {
            delay(1000)
            try {
                screenState.value = MainScreenState.Success(repository.getData(status))
            } catch (e: Exception) {
                screenState.value = MainScreenState.Error(defaultError)
            }
        }
    }
}