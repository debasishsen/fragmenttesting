package com.example.fragmenttesting

interface MainRepository {
    suspend fun getData(status: Boolean): UserModel
}

class MainRepositoryImpl : MainRepository {

    private val api: MainApi = object : MainApi {
        override suspend fun getData(): UserModel {
            return UserModel("John Doe", 25, "super_mike@gmail.com")
        }
    }

    override suspend fun getData(status: Boolean): UserModel {
        if (status) {
            return api.getData()
        } else {
            throw Exception()
        }
    }
}