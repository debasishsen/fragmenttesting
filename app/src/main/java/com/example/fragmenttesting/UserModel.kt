package com.example.fragmenttesting

data class UserModel(
    val name: String,
    val age: Int,
    val email: String
)