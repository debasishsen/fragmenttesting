package com.example.fragmenttesting

interface MainApi {
    suspend fun getData(): UserModel
}