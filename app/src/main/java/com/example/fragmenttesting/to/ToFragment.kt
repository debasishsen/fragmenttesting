package com.example.fragmenttesting.to

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material.MaterialTheme
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.fragmenttesting.Constants
import com.example.fragmenttesting.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ToFragment : Fragment() {

    private val args : ToFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                MaterialTheme {
                    ToScreen(onClick = { navigateToDetailFragment() }, args.text)
                }
            }
        }
    }

    private fun navigateToDetailFragment(){
            findNavController().navigate(R.id.detailFragment)
    }
}