package com.example.fragmenttesting.to

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.example.fragmenttesting.R

@Composable
fun ToScreen(onClick: () -> Unit = {}, text: String = "SecondFragment") {

        ToView(onClick, text)

}

@Composable
fun ToView(onClick: () -> Unit = {}, text: String) {

        Column(
                modifier = Modifier.fillMaxWidth().fillMaxHeight(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
        ) {
                Button(onClick = {
                        onClick.invoke()
                }) {
                        Text(text)
                }
        }
}