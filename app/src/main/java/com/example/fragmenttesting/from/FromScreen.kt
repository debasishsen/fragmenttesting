package com.example.fragmenttesting.from

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.fragmenttesting.R

@Composable
fun FromScreen(onClickCheck: (type: String) -> Unit = {}, value: String){
        SuccessView(onClickCheck, value)
}

@Composable
fun SuccessView(onClickCheck: (type: String) -> Unit = {}, value: String) {
        var textValue by remember { mutableStateOf(TextFieldValue("")) }
        Column(
                modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
        ) {
                Text(
                        text = value,
                        textAlign = TextAlign.Center,
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Bold,
                        color = Color.DarkGray
                )
                BasicTextField(
                        value = textValue,
                        modifier = Modifier
                                .padding(16.dp)
                                .fillMaxWidth()
                                .testTag("EditTextTestTag"),
                        onValueChange = {
                                textValue = it
                        }
                )
                Button(onClick = {
                        onClickCheck(textValue.text)
                }) {
                        Text("From")
                }
        }
}

@Composable
fun ErrorView(value : String){
        Column(
                modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
        ){

                Text(
                        text = value,
                        textAlign = TextAlign.Center,
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Bold,
                        color = Color.DarkGray
                )
        }
}