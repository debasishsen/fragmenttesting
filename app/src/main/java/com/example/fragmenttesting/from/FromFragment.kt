package com.example.fragmenttesting.from

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.ComposeView
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import com.example.fragmenttesting.Constants
import com.example.fragmenttesting.MainScreenState
import com.example.fragmenttesting.MainViewModel
import com.example.fragmenttesting.R

@AndroidEntryPoint
class FromFragment : Fragment() {

    val mainViewModel by viewModels<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                mainViewModel.fetchData(true)
                MaterialTheme {
                    val state = mainViewModel.screenState.observeAsState().value
                    when(state){
                        is MainScreenState.Success -> FromScreen(
                            onClickCheck = {
                                onClickCheck(type = it)
                            },
                            value = state.data.name
                        )
                        is MainScreenState.Error -> ErrorView(value = state.message)
                        is MainScreenState.Loading -> showStatus("Loading")
                    }
                }
            }
        }
    }

    private fun showStatus(msg: String){
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    private fun onClickCheck(type: String){
            findNavController().navigate(("${Constants.PROFILE_DEEP_LINK}$type").toUri())
//            findNavController().navigate(R.id.toFragment)
    }
}