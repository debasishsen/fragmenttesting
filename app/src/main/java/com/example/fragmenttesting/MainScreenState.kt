package com.example.fragmenttesting

sealed class MainScreenState {
    data class Success(val data: UserModel) : MainScreenState()
    data class Error(val message: String) : MainScreenState()
    object Loading : MainScreenState()
}