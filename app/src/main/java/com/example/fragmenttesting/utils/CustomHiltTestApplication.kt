package com.example.fragmenttesting.utils

import android.app.Application
import dagger.hilt.android.testing.CustomTestApplication

// Use default HiltTestApplication instead for customerTestRunner
@CustomTestApplication(Application::class)
interface CustomHiltTestApplication